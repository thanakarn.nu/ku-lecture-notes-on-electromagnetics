
|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |   

|[ก่อนหน้า](ch1-06-01TH.md)| [ต่อไป](ch1-06-03TH.md) |
| ---------- | ---------- |    

### 1.6.2 [ตัวอย่างการหาพื้นที่ครึ่งวงกลม](ch1-06-02TH.md)  
จงหาพื้นที่ของครึ่งวงกลมที่มีรัศมีเท่ากับ $`a`$  
#### วิธีทำ  
สมการวงกลมที่มีจุดศูนย์กลางอยู่ที่จุดกำเนิดและมีรัศมีเท่ากับ $`a`$ คือ $`x^2+y^2=a^2`$ หรือเขียนในพิกัดทรงกระบอกโดยแทนค่า $`\rho^2 = x^2+y^2`$ ได้ดังนี้ $`\rho^2=a^2`$ หรือ $`\rho=a`$  
เราให้พื้นที่ดิฟเฟอเรนเชียล หรือพื้นที่เล็กที่สุดโดยแบ่งในแนว $`\rho`$ กับ $`\phi`$ เราจะได้ว่า $`ds = \rho d \rho d\phi`$ 
ถ้าเราแบ่งพื้นที่ของครึ่งวงกลมเป็นสี่เหลี่ยมชิ้นเล็กๆ โดยมีการขนาดการเปลี่ยนแปลง $`\Delta \rho`$ เท่ากันทุกชิ้น และ ขนาดการเปลี่ยนแปลง $`\Delta \phi`$ เท่ากันทุกชิ้น ขนาดของสี่เหลี่ยมชิ้นเล็กๆเท่ากับ $`\Delta s_k = \rho_i \Delta \rho_i \Delta \phi_j`$ แล้วเอาพื้นที่ของสี่เหลี่ยมแต่ละชิ้นมาร่วมกันทั้งหมด เราจะได้พื้นที่ของครึ่งวงกลมโดยประมาณ 
```math
S \approx \sum_{i=1}^{i=N_{\rho}} \sum_{j=1}^{j= N_{\phi}} { \rho_i \Delta \phi_j \Delta \rho_i}
```  
```math
S \approx \sum_{i=1}^{i=N_{\rho}} \sum_{j=1}^{j= N_{\phi}} {  \Delta \phi_j \rho_i \Delta \rho_i}
```  
```math
S \approx \sum_{i=1}^{i=N_{\rho}} \left( N_{\phi} \Delta \phi \right) \rho_i \Delta \rho_i
```  
$` \left( N_{\phi} \Delta \phi \right)`$ มีค่าเท่ากับ $`\pi`$ เพราะเราคิดแค่**ครึ่งวงกลม**  
```math
S \approx \sum_{i=1}^{i=N_{\rho}}  \pi \rho_i \Delta \rho_i
```  
```math
S \approx \pi\Delta \rho \sum_{i=1}^{i=N_{\rho}}   \rho_i 
```  
$`\rho_i`$ คือจุดกึ่งกลางของแต่ละชิ้นดังนั้น $`\rho_i = \Delta \rho \left(i-0.5\right)`$ เราจึงได้สมการเป็น   
```math
S \approx \pi\Delta \rho \sum_{i=1}^{i=N_{\rho}}  \Delta \rho \left(i-0.5\right)
```  
```math
S \approx \pi\Delta \rho^2 \sum_{i=1}^{i=N_{\rho}}   \left(i-0.5\right)
```  
```math
S \approx \pi\Delta \rho^2 \left(\frac {N_{\rho}}{2} \left( N_{\rho}+1\right)-0.5 N_{\rho} \right)
```  
```math
S \approx \pi\Delta \rho \left(N_{\rho} \Delta \rho \right) \left(\frac {1}{2} \left( N_{\rho}+1\right)-0.5  \right)
```  
```math
S \approx \pi\Delta \rho \left(N_{\rho} \Delta \rho \right) \left(\frac { N_{\rho}}{2}   \right)
```  
```math
S \approx \pi \left(N_{\rho} \Delta \rho \right)^2 \frac{1}{2}
```  
$`N_{\rho} \Delta \rho`$ มีค่าเท่ากับรัศมีของวงกลมคือ $`a`$ ดังนี้
```math
S \approx \frac{\pi a^2}{2}
```  
เนื่องจากเราประมาณ $`\rho_i \Delta \phi_j \Delta \rho_i`$ เป็นพื้นที่ของสี่เหลี่ยมมุมฉากที่มีด้านเท่ากับ $`\rho_i  \Delta \rho_i`$ และ $` \Delta \rho_i`$ ซึ่งมีค่าเท่ากับพื้นที่ของสี่เหลี่ยมที่มีด้านเป็นส่วนโค้งของวงกลมพอดี ดังนั้นค่าประมาณจึงเท่ากับค่าจริงพอดีแต่ค่าประมาณไม่จำเป็นต้องเท่ากับค่าจริงเสมอไป ซึ่งถ้าเราให้ค่า  $` \Delta \rho`$ และ  $` \Delta \phi`$ มีขนาดเล็กลงจะทำให้ค่าประมาณใกล้เคียงกับค่าจริงมากขึ้น ถ้าเราให้ค่าลู่เข้าศูนย์เราจะได้ค่าจริง แต่เราจะมีจำนวนพื้นที่เล็กๆเป็นอนันต์ 
ซึ่งเราไม่สามารถเอาค่ามารวมกันเป็นอนันต์(infinite) ครั้ง แต่เราจะเปลี่ยนเครื่องหมาย summation เป็นเครื่องหมาย integrate แทนและเปลี่ยน $`{\Delta \rho_i}`$ เป็น $`{d\rho}`$ และ $`{\Delta \phi_j}`$ เป็น $`{d\phi}`$ ดังนั้น 
```math
S = \sum_{i=1}^{i=\infty} \sum_{j=\phi_{start(i)}}^{j= \phi_{stop(i)}} { \rho_i \Delta \phi_j \Delta \rho_i} = \int_{\rho=initial}^{\rho = final} \int_{\phi=g(\rho)}^{\phi = f(\rho)} \rho d\phi d\rho
```  
โดย $`initial`$ เป็นตำแหน่ง $`i=1`$ และ $`final`$ เป็นตำแหน่ง $`i=\infty`$ และค่าของ $`f(\rho)`$ และ $`g(\rho)`$ ขึ้นอยู่กับค่าของ $`\rho`$  
จากสมการวงกลมเราจะได้ $`g(\rho) = 0`$ และ $`f(\rho) = \pi`$ และ $`initial = 0 `$ และ $`final = a`$ เราจะได้สมการปริพันธ์ดังนี้  
```math
S = \int_{\rho=0}^{\rho= a} \int_{\phi=0}^{\phi = \pi} \rho d\phi d\rho \newline  
S = \int_{\rho=0}^{\rho= a} \phi \bigg|_0^{\pi} \rho d\rho \newline
S = \int_{\rho=0}^{\rho= a} \pi \rho d\rho \newline
S = \pi \frac{\rho^2}{2}\bigg|_{\rho=0}^{\rho= a} \newline
S =  \frac{\pi a^2}{2} \newline
```  
ถ้าเรากลับไปดูตัวอย่างที่คำนวนด้วยพิกัดฉากเราจะเห็นว่าเราได้คำตอบเหมือนกันแต่ สมการอินทิเกรตยากกว่ามาก เราจึงสมควรพิจารณาปัญหาก่อนว่า ปัญหามีลักษณะคล้ายกับพิดแบบใด ถ้าเราเลือกใช้พิกัดได้ถูกกับปัญหา เราก็จะสามารถแก้ปัญหาได้ง่ายขึ้น   


|[ก่อนหน้า](ch1-06-01TH.md)| [ต่อไป](ch1-06-03TH.md) |
| ---------- | ---------- |    

