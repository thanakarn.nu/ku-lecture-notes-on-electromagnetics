[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | -------- |    

|[ก่อนหน้า](../contentTH.md)| [ต่อไป](ch1-01TH.md) |
| ---------- | ---------- |    

# บทที่ 1: การวิเคราะห์เวกเตอร์ และ ระบบพิกัด  
บทนี้จะกล่าวถึงเนื้อหาเกี่ยวกับเวกเตอร์แบบสั้นๆ เพื่อที่จะสามารถใช้ในการเรียนด้านวิศวกรรมสนามแม่เหล็กไฟฟ้าได้ เนื่องจากสนามแม่เหล็กไฟฟ้ามีทั้งขนาดและทิศทาง เวลาคำนวนหาสนามแม่เหล็กไฟฟ้าที่เกิดจากหลายแหล่งกำเนิด จะต้องนำปริมาณที่หาได้มารวมกันแบบเวกเตอร์
## [1.1 ปริมาณสเกลาร์และเวกเตอร์ (Scalar and Vector Quantities)](ch1-01TH.md)  
## [1.2 พีชคณิตเวกเตอร์ (Vector Algebra)](ch1-02TH.md)  
## [1.3 ระบบพิกัดแบบคาร์ทีเซียน หรือ ระบบพิกัดฉาก (Cartesian Coordinate System)](ch1-03TH.md)  
## [1.4 ส่วนประกอบเวกเตอร์และเวกเตอร์หนึ่งหน่วย (Vector Components and Unit Vectors)](ch1-04TH.md)  
## [1.5 การคูณเวกเตอร์ (Vector Multiplication)](ch1-05TH.md)  
## [1.6 พิกัดทรงกระบอก (Circular Cylindrical Coordinates)](ch1-06TH.md)  
## [1.7 พิกัดทรงกลม (Spherical Coordinates)](ch1-07TH.md)  
## [1.8 การแปลงเวกเตอร์ระหว่างพิกัดต่างๆ (Coordinate Transformation of Vector Components)](ch1-08TH.md)  


|[ก่อนหน้า](../contentTH.md)| [ต่อไป](ch1-01TH.md) |
| ---------- | ---------- |    


[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


